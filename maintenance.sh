#!/bin/bash
#THIS SCRIPT IS TO AUTO INSTALL ALL OF MY USUAL DEVELOPMENT TOOLS.
#Written by Delicia Brummitt (dkbrummitt@gmail.com)
clear
##########################
# Help
##########################
if [ "$1" = "-h" ] 
then
	echo =====================================================
	echo "This script will upgrade/install any or all of the following to /usr/local/ :"
	echo "		* Java "
	echo "		* ApacheTomcat"
	echo "		* Tanuki Service Wrapper for Tomcat" 
	echo "provided that the necessary installation files are available, and the user has root access."
	echo 
	echo "You will need:"
	echo "		* Java installtion RPM file"
	echo "		* Apache Tomcat TAR.GZ installtion file"
	echo "		* Tanuki Service Wrapper for Tomcat TAR.GZ installtion file"
	echo
	echo "Steps:"
	echo "		1. Ensure that you are the Super User [root] on the installation box."
	echo "		2. Place the installtion file[s] in the same directory as this script."
	echo "		3. Have the installation file name[s] readily available for when you are prompted for them."
	echo "		4. Run the shell script."
	echo
	echo "NOTES:"
	echo "		* This script will set/reset various garbage collection and memory options for tomcat."
	echo "		  It will also turn on JMX monitoring for Tomcat via JConsole, and can be accessed "
	echo "		  via Port 9012"
	echo "		* This script will add Sym Links for Java and Tomcat if they do not exist already."
	echo "		  It will also chnage any Sym Links in place, but only upon aproval"
	echo "		* The Java RPM will install java to /usr/java."
	echo "		* When installing Tomcat, you will be prompted with the port that you wish to use"
	echo "		  for the installation. The default is 8080."
	echo "		* Usergroup and user 'tomcat' will be created if it doesnt not already exist. "
	echo "		  Also, the tomcat installation will be owned by the tomcat user."
	echo "		* This script will test the tomcat installations via at least one of the following:"
	echo "			- start"
	echo "			- restart"
	echo "			- stop"
	echo "		* This script will configure the wrapper.conf per current IBBS standards; "
	echo "		  With the addition of JMX monitoring. The settings will appear on screen at the "
	echo "		  time they are addjusted/set"
	echo "		WARNING:"
	echo "		  This script will add/adjust the Sym Link for /etc/init.d/tomcat without prompting "
	echo "		  to the new installation.  You will have to re-adjust to Undo."
	echo =====================================================
	echo
	exit 1
else
	echo "Run with -h if you need help running this script"	echo
	echo =====================================================
	echo
fi

TEMP=`readlink -f $0`
BASE_DIR=`dirname $TEMP`
TOMCAT_INST=null
JAVA_INST=null
WRAPPER_INST=null
TOMCAT_DIR=null
JAVA_DIR=null
WRAPPER_DIR=null
 
##########################
# Install Java
##########################
read -p 'Do you want to install/upgrade Java? [y, n] ' YES_NO
if [ "$YES_NO" = "y" ]
then	
	read -p 'Enter the Java installation RPM file name : ' JAVA_INST
	
	TEMP=`expr length $JAVA_INST - 7`
	JAVA_DIR=`echo $JAVA_INST | cut -c 1-$TEMP`
	chmod a+x $JAVA_INST
	
	echo `date` Installing $JAVA_INST ...
	
	read -p 'Check script: Correct Dir Name? ' hmmm
	cp $JAVA_INST /usr/local/
	read -p 'Check script: Copied RPM to user local ? ' hmmm
	cd /usr/local/
	./$JAVA_INST
		JAVA_DIR=`ls -tr /usr/java/|grep jdk|tail -n 1`
		JAVA_DIR=/usr/java/$JAVA_DIR
		echo Installed to $JAVA_DIR
	
	read -p 'Check script ? ' hmmm
	rm -f /usr/local/*rpm*
	read -p 'Check script ? ' hmmm

	if [ -L /usr/bin/java ]
	then
		read -p 'Do you want to replace Java SymLink in /usr/bin/java with a link to this java install? [y, n] ' YES_NO
		if [ "$YES_NO" = "y" ]
		then
			cd /usr/bin/
			echo `date` Replacing previous Java SymLink in /usr/bin/java 
			echo `ls -la /usr/bin |grep java`
			rm -rfd /usr/bin/java
			ln -s $JAVA_DIR/bin/java java
		fi
		
	fi
	if [ -L /usr/sbin/java ]
	then
		read -p 'Do you want to replace Java SymLink in /usr/sbin/java with a link to this java install? [y, n] ' YES_NO
		if [ "$YES_NO" = "y" ]
		then
			cd /usr/sbin/
			echo `date` Replacing previous Java SymLink in /usr/sbin/java 
			echo `ls -la /usr/sbin/ |grep java`
			rm -rfd /usr/sbin/java
			ln -s $JAVA_DIR/bin/java java
		fi
	fi
	if [ -L /usr/local/java ]
	then
		read -p 'Do you want to replace Java SymLink in /usr/local/java with a link to this java install? [y, n] ' YES_NO
		if [ "$YES_NO" = "y" ]
		then
			cd /usr/local/
			echo `date` Replacing previous Java SymLink in /usr/local/java 
			echo `ls -la /usr/local/ |grep java`
			rm -rfd /usr/local/java
			ln -s $JAVA_DIR java
		fi
	fi	
	cd $BASE_DIR
fi

##########################
# Install Apache Tomcat
##########################
read -p 'Do you want to install/upgrade Apache Tomcat? [y, n] ' YES_NO
if [ "$YES_NO" = "y" ]
then
	read -p 'Enter the .TAR.GZ Installation file for Apache Tomcat: ' TOMCAT_INST
	cd /usr/local/
	cp $BASE_DIR/$TOMCAT_INST /usr/local/
	echo
	
	echo `date` Expanding Tomcat Tar...
	tar -xzvf $TOMCAT_INST
	echo
	
	echo `date` Setting Access Rights
	TOMCAT_DIR=`ls -tr |grep tomcat|tail -n 1`
	TOMCAT_DIR=/usr/local/$TOMCAT_DIR
	rm -rfd /usr/local/$TOMCAT_INST
	chmod 755 $TOMCAT_DIR/bin/*.sh
	echo
	
	echo `date` Adjusting Tomcat properties...
	echo  'org.apache.jasper.compiler.Parser.STRICT_QUOTE_ESCAPING=false'
	cat $TOMCAT_DIR/conf/catalina.properties | sed -e '/org.apache.jasper.compiler.Parser.STRICT_QUOTE_ESCAPING/d' > $TOMCAT_DIR/js-wrapper/bin/tomcat				
	echo "org.apache.jasper.compiler.Parser.STRICT_QUOTE_ESCAPING=false" >> $TOMCAT_DIR/conf/catalina.properties

	#TODO: Figure out how to set Catalina OPTS from script correctly.  
	#CATALINA_OPTS="-Dcom.sun.management.jmxremote/-Dcom.sun.management.jmxremote.port=8999/-Dcom.sun.management.jmxremote.ssl=false/-Dcom.sun.management.jmxremote.authenticate=false"
	#sed -i 'CATALINA_OPTS="$CATALINA_OPTS" $CATALINA_OPTS' $TOMCAT_DIR/bin/catalina.sh
	#export $CATALINA_OPTS
	#echo $CATALINA_OPTS

	read -p 'Do you want to set the Port used by this Tomcat Installation[Default is 8080]? [y, n] ' YES_NO
	if [ "YES_NO" = "y" ]
	then
		read -p 'Enter the port that you wish to use: ' PORT
		cat $TOMCAT_DIR/conf/server.xml | sed -e 's/8080/$PORT/' > $TOMCAT_DIR/conf/server.xml
	fi
	
	echo `date` Preparing JMX Monitoring pass word access...
	echo "monitorRole readonly" > $TOMCAT_DIR/conf/jmxremote.access
	echo "controlRole readwrite" >> $TOMCAT_DIR/conf/jmxremote.access
	echo "monitorRole tomcat" > $TOMCAT_DIR/conf/jmxremote.password
	echo "controlRole tomcat" >> $TOMCAT_DIR/conf/jmxremote.password
	chmod 600 $TOMCAT_DIR/conf/jmxremote.password
	

	read -p 'Do you adjust the Sym Link in /usr/local? [y, n] ' YES_NO
	if [ "YES_NO" = "y" ]
	then
		echo `date` Linking new Directory...
		if [ -L /usr/local/tomcat ]
		then
			echo `date` Removing previous tomcat SymLink
			rm -rfd /usr/local/tomcat
			ln -s $TOMCAT_DIR tomcat
			echo
		else
			echo `date` Cant remove /usr/local/tomcat since may be a real Directory and not a Symlink
		fi
	fi		
	echo `date` Checking Group and User Rights...
	TEMP=`egrep -i "^tomcat" /etc/group`
	if [ -z TEMP ]
	then
		echo Creating tomcat user group...
		/usr/sbin/groupadd tomcat
	fi
	TEMP=`egrep -i "^tomcat" /etc/passwd`
	
	if [ -z TEMP ]
	then
		echo Creating tomcat user...
		/usr/sbin/useradd tomcat -c "Apache Tomcat" -d $TOMCAT_DIR -g tomcat -s /bin/bash
	fi
	chown -R tomcat:tomcat $TOMCAT_DIR
	chown tomcat:tomcat tomcat
	echo

#TODO migrate feature
#	read -p 'Do you want migrate any web apps to this installation? [y, n] ' YES_NO
#	if [ "YES_NO" = "y" ]
#	then
#		while [ "YES_NO" = "y" ]
#		do
#		read -p 'Enter the path to the Web App Directory you wish to migrate from? [e.g., /usr/local/tomcat5] ' M_APP
#			
#		read -p 'Do you want migrate more web apps to this installation? [y, n] ' YES_NO
#		done
#		read -p 'Enter the port that you wish to use: ' PORT
#		cat $TOMCAT_DIR/conf/server.xml | sed -e 's/8080/$PORT/' > $TOMCAT_DIR/conf/server.xml
#	fi
	
	
	echo `date` Testing Tomcat $TOMCAT_DIR Installation via bin
	cd $TOMCAT_DIR/bin/
	./startup.sh
	sleep 1
	./shutdown.sh
	echo
	cd $BASE_DIR
fi

##########################
# Tomcat Service Wrapper
##########################
read -p 'Do you want to install/upgrade Apache Tomcat Service Wrapper? [y, n] ' YES_NO
if [ $YES_NO = "y" ]
then
        read -p 'Enter the ServiceWrapper Intall file[.tar.gz]:' WRAPPER_INST
        cd /usr/local/
        cp $BASE_DIR/$WRAPPER_INST /usr/local/
        echo `date` Expanding Service Wrapper Tar...
        tar -xzvf $WRAPPER_INST
	rm -rfd /usr/local/$WRAPPER_INST
        TEMP=`expr length $WRAPPER_INST - 7`
        WRAPPER_DIR=`echo $WRAPPER_INST | cut -c 1-$TEMP`
        chmod 755 /usr/local/$WRAPPER_DIR/bin/*.sh
	echo
	
	if [ -z $TOMCAT_DIR -o "$TOMCAT_DIR" = "null" ]
	then
		TOMCAT_DIR=ls -tr /usr/local/|grep tomcat|tail -n 1
		TOMCAT_DIR=/usr/local/$TOMCAT_DIR
	fi
        echo `date` Creating Wrapper Directories in $TOMCAT_DIR...
        cd $TOMCAT_DIR	
	
	if [ ! -e $TOMCAT_DIR/js-wrapper ]
	then
	        echo `date` Creating Wrapper Directories in $TOMCAT_DIR...
	        mkdir  js-wrapper
	        mkdir  js-wrapper/bin
	        mkdir  js-wrapper/conf
	        mkdir  js-wrapper/lib
	        mkdir  js-wrapper/logs
	fi
	echo `date` Copying necessary files from $WRAPPER_DIR to $TOMCAT_DIR/js-wrapper...
       	cp /usr/local/$WRAPPER_DIR/bin/wrapper $TOMCAT_DIR/js-wrapper/bin/
       	cp /usr/local/$WRAPPER_DIR/lib/wrapper.jar $TOMCAT_DIR/js-wrapper/lib
 	cp /usr/local/$WRAPPER_DIR/lib/libwrapper.so $TOMCAT_DIR/js-wrapper/lib
       	cp /usr/local/$WRAPPER_DIR/src/conf/wrapper.conf.in $TOMCAT_DIR/js-wrapper/conf/wrapper.conf
       	cp /usr/local/$WRAPPER_DIR/src/bin/sh.script.in $TOMCAT_DIR/js-wrapper/bin/tomcat

        echo `date` Setting Wrapper Access Rights...
	chown -R tomcat:tomcat js-wrapper
	VERSION=$TOMCAT_DIR | sed 's/usr\/local\/apache-tomcat-//'
        chmod 755 $TOMCAT_DIR/js-wrapper/bin/tomcat	
	echo
	
	echo `date` Editing $TOMCAT_DIR/js-wrapper/bin/tomcat to reflect
        echo APP_NAME="tomcat"
        echo APP_LONG_NAME="Apache Tomcat $VERSION"
        echo RUN_AS_USER=tomcat uncomment this line

        cat $TOMCAT_DIR/js-wrapper/bin/tomcat | sed -e 's/@app.name@/tomcat/' | sed -e 's/@app.long.name@/Apache Tomcat $VERSION/' | sed -e '/#RUN_AS_USER/d' > $TOMCAT_DIR/js-wrapper/bin/tomcat				
	"RUN_AS_USER=tomcat" >>  $TOMCAT_DIR/js-wrapper/bin/tomcat 

	echo `date` Editing $TOMCAT_DIR/js-wrapper/conf/wrapper.conf to reflect
	cat $TOMCAT_DIR/js-wrapper/conf/wrapper.conf | sed -e '/wrapper.java.command/d'| sed -e '/wrapper.java.mainclass/d'| sed -e '/wrapper.java.mainclass/d'| sed -e '/wrapper.java.classpath.1/d'| sed -e '/wrapper.java.classpath.2/d'| sed -e '/wrapper.java.additional.1/d' > $TOMCAT_DIR/js-wrapper/conf/wrapper.conf
	echo "wrapper.java.command=/usr/local/java/bin/java" >> $TOMCAT_DIR/js-wrapper/conf/wrapper.conf
	echo "wrapper.java.mainclass=org.tanukisoftware.wrapper.WrapperStartStopApp" >> $TOMCAT_DIR/js-wrapper/conf/wrapper.conf
        echo "wrapper.java.classpath.1=../lib/wrapper.jar" >> $TOMCAT_DIR/js-wrapper/conf/wrapper.conf
        echo "wrapper.java.classpath.2=/usr/local/java/lib/tools.jar" >> $TOMCAT_DIR/js-wrapper/conf/wrapper.conf
        echo "wrapper.java.classpath.3=$TOMCAT_DIR/bin/bootstrap.jar" >> $TOMCAT_DIR/js-wrapper/conf/wrapper.conf
        echo "wrapper.java.additional.1=-Djava.util.logging.manager=org.apache.juli.ClassLoaderLogManager" >> $TOMCAT_DIR/js-wrapper/conf/wrapper.conf
        echo "wrapper.java.additional.2=-Djava.util.logging.config.file=$TOMCAT_DIR/conf/logging.properties" >> $TOMCAT_DIR/js-wrapper/conf/wrapper.conf
        echo "wrapper.java.additional.3=-Djava.endorsed.dirs=$TOMCAT_DIR/endorsed" >> $TOMCAT_DIR/js-wrapper/conf/wrapper.conf
        echo "wrapper.java.additional.4=-Dcatalina.base=$TOMCAT_DIR" >> $TOMCAT_DIR/js-wrapper/conf/wrapper.conf
        echo "wrapper.java.additional.5=-Dcatalina.home=$TOMCAT_DIR" >> $TOMCAT_DIR/js-wrapper/conf/wrapper.conf
        echo "wrapper.java.additional.6=-Djava.io.tmpdir=$TOMCAT_DIR/TEMP" >> $TOMCAT_DIR/js-wrapper/conf/wrapper.conf
	echo "wrapper.java.additional.7=-XX:PermSize=384m" >> $TOMCAT_DIR/js-wrapper/conf/wrapper.conf
	echo "wrapper.java.additional.8=-XX:MaxPermSize=384m" >> $TOMCAT_DIR/js-wrapper/conf/wrapper.conf
	echo "wrapper.java.additional.9=-verbose:gc" >> $TOMCAT_DIR/js-wrapper/conf/wrapper.conf
	echo "wrapper.java.additional.10=-XX:+PrintGCTimeStamps" >> $TOMCAT_DIR/js-wrapper/conf/wrapper.conf
	echo "wrapper.java.additional.11=-XX:+PrintGCDetails" >> $TOMCAT_DIR/js-wrapper/conf/wrapper.conf
	echo "wrapper.java.additional.12=-XX:+PrintHeapAtGC" >> $TOMCAT_DIR/js-wrapper/conf/wrapper.conf
	echo "wrapper.java.additional.13=-XX:+UseConcMarkSweepGC" >> $TOMCAT_DIR/js-wrapper/conf/wrapper.conf
	echo "wrapper.java.additional.14=-Dcom.sun.management.jmxremote" >> $TOMCAT_DIR/js-wrapper/conf/wrapper.conf
	echo "wrapper.java.additional.15=-Dcom.sun.management.jmxremote.port=9012" >> $TOMCAT_DIR/js-wrapper/conf/wrapper.conf
	echo "wrapper.java.additional.16=-Dcom.sun.management.jmxremote.authenticate=TRUE" >> $TOMCAT_DIR/js-wrapper/conf/wrapper.conf
	echo "wrapper.java.additional.17=-Dcom.sun.management.jmxremote.password.file=$TOMCAT_DIR/conf/jmxremote.password" >> $TOMCAT_DIR/js-wrapper/conf/wrapper.conf
	echo "wrapper.java.additional.18=-Dcom.sun.management.jmxremote.access.file=$TOMCAT_DIR/conf/jmxremote.access" >> $TOMCAT_DIR/js-wrapper/conf/wrapper.conf
	echo "wrapper.java.additional.19=-Dorg.tanukisoftware.wrapper.WrapperManager.mbean=TRUE" >> $TOMCAT_DIR/js-wrapper/conf/wrapper.conf
	echo "wrapper.java.additional.20=-Dorg.tanukisoftware.wrapper.WrapperManager.mbean.testing=FALSE" >> $TOMCAT_DIR/js-wrapper/conf/wrapper.conf
	echo "wrapper.java.initmemory=1024" >> $TOMCAT_DIR/js-wrapper/conf/wrapper.conf
	echo "wrapper.java.maxmemory=2048" >> $TOMCAT_DIR/js-wrapper/conf/wrapper.conf
        echo "wrapper.app.parameter.1=org.apache.catalina.startup.Bootstrap" >> $TOMCAT_DIR/js-wrapper/conf/wrapper.conf
        echo "wrapper.app.parameter.2=1" >> $TOMCAT_DIR/js-wrapper/conf/wrapper.conf
        echo "wrapper.app.parameter.3=start" >> $TOMCAT_DIR/js-wrapper/conf/wrapper.conf
        echo "wrapper.app.parameter.4=org.apache.catalina.startup.Bootstrap" >> $TOMCAT_DIR/js-wrapper/conf/wrapper.conf
        echo "wrapper.app.parameter.5=TRUE" >> $TOMCAT_DIR/js-wrapper/conf/wrapper.conf
        echo "wrapper.app.parameter.6=1" >> $TOMCAT_DIR/js-wrapper/conf/wrapper.conf
        echo "wrapper.app.parameter.7=stop" >> $TOMCAT_DIR/js-wrapper/conf/wrapper.conf
        echo "wrapper.logfile.maxsize=8m" >> $TOMCAT_DIR/js-wrapper/conf/wrapper.conf
        echo "wrapper.logfile.maxfiles=10" >> $TOMCAT_DIR/js-wrapper/conf/wrapper.conf

	#NOTE: Wrapper could send emails if Pro version is perchased.
	echo "wrapper.java.command=/usr/local/java/bin/java" 
	echo "wrapper.java.mainclass=org.tanukisoftware.wrapper.WrapperStartStopApp" 
        echo "wrapper.java.classpath.1=$TOMCAT_DIR/js-wrapper/lib/wrapper.jar" 
        echo "wrapper.java.classpath.2=/usr/local/java/lib/tools.jar" 
        echo "wrapper.java.classpath.3=$TOMCAT_DIR/bin/bootstrap.jar" 
        echo "wrapper.java.additional.1=-Djava.util.logging.manager=org.apache.juli.ClassLoaderLogManager" 
        echo "wrapper.java.additional.2=-Djava.util.logging.config.file=$TOMCAT_DIR/conf/logging.properties" 
        echo "wrapper.java.additional.3=-Djava.endorsed.dirs=$TOMCAT_DIR/endorsed" 
        echo "wrapper.java.additional.4=-Dcatalina.base=$TOMCAT_DIR" 
        echo "wrapper.java.additional.5=-Dcatalina.home=$TOMCAT_DIR" 
        echo "wrapper.java.additional.6=-Djava.io.tmpdir=$TOMCAT_DIR/TEMP" 
	echo "wrapper.java.additional.7=-XX:PermSize=384m" 
	echo "wrapper.java.additional.8=-XX:MaxPermSize=384m" 
	echo "wrapper.java.additional.9=-verbose:gc" 
	echo "wrapper.java.additional.10=-XX:+PrintGCTimeStamps" 
	echo "wrapper.java.additional.11=-XX:+PrintGCDetails" 
	echo "wrapper.java.additional.12=-XX:+PrintHeapAtGC" 
	echo "wrapper.java.additional.13=-XX:+UseConcMarkSweepGC" 
	echo "wrapper.java.additional.14=-Dcom.sun.management.jmxremote" 
	echo "wrapper.java.additional.15=-Dcom.sun.management.jmxremote.port=9012" 
	echo "wrapper.java.additional.16=-Dcom.sun.management.jmxremote.authenticate=TRUE" 
	echo "wrapper.java.additional.17=-Dcom.sun.management.jmxremote.password.file=$TOMCAT_DIR/conf/jmxremote.password" 
	echo "wrapper.java.additional.18=-Dcom.sun.management.jmxremote.access.file=$TOMCAT_DIR/conf/jmxremote.access" 
	echo "wrapper.java.additional.19=-Dorg.tanukisoftware.wrapper.WrapperManager.mbean=TRUE" 
	echo "wrapper.java.additional.20=-Dorg.tanukisoftware.wrapper.WrapperManager.mbean.testing=FALSE" 
	echo "wrapper.java.initmemory=1024" 
	echo "wrapper.java.maxmemory=2048" 
        echo "wrapper.app.parameter.1=org.apache.catalina.startup.Bootstrap" 
        echo "wrapper.app.parameter.2=1" 
        echo "wrapper.app.parameter.3=start" 
        echo "wrapper.app.parameter.4=org.apache.catalina.startup.Bootstrap" 
        echo "wrapper.app.parameter.5=TRUE" 
        echo "wrapper.app.parameter.6=1" 
        echo "wrapper.app.parameter.7=stop" 
        echo "wrapper.logfile.maxsize=8m" 
        echo "wrapper.logfile.maxfiles=10" 

        read -p 'Adjust Symlink to this service wrapper [y, n]? ' YES_NO
        if [ $YES_NO="y" ]
	then

                echo `date` Creating Tomcat Service Wrapper symlink ...
                cd /etc/init.d
                if [ -L /etc/init.d/tomcat -o ! -e /etc/init.d/tomcat ]
		then
		        echo `date` Removing previous tomcat service wrapper SymLink
		        rm -rfd /etc/init.d/tomcat
			cd /etc/init.d
			ln -s /usr/local/tomcat/js-wrapper/bin/tomcat tomcat
			#ln -s $TOMCAT_DIR/js-wrapper/bin/tomcat tomcat

		        CHK_OK=`/sbin/chkconfig --list tomcat`
		        if [ -z $CHK_OK ]
			then
				/sbin/chkconfig --add tomcat
			        /sbin/chkconfig --level 3 tomcat on
				echo `date` Added tomcat to chkconfig
			fi
			
			echo `date` Testing Service Wrapper from /etc/init.d ...
			cd /etc/init.d
		        ./tomcat start
	        	./tomcat restart
	        	./tomcat stop

		else
		        echo `date` Cant remove /etc/init.d/tomcat since it is a Real Directory and not a Symlink
		fi
	
	fi	
	echo `date` Testing Service Wrapper from $TOMCAT_DIR...
	cd $TOMCAT_DIR/js-wrapper/bin
        ./tomcat start
        ./tomcat restart
        ./tomcat stop        
fi
echo `date` Done!

