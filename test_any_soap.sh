#!/bin/bash

# This shell script allows the user being able to send canned 
# SOAP XML transactions to aid in Integration Tests.  The script takes inputs
# in any order and can be pointed at local applications or remote ones.  It
# is designed to be with a soapclient.sh file which will send the XMLs to the 
# directed host using Java HTTP client
# 
# Long term plans for this script include using it with python or converting it
# to python and using it for fully automated integration testing of SOAP 
# applications.
#

clear
cd /usr/local/tomcat5/webapps/axis/WEB-INF/classes
XML=../xml/getVersion.xml
CLIENT=""
HOST="http://localhost:8080"
APP="axis"
#Parse params to ID the test XML, CLIENT, and HOST...
for f in $*
        do
                if [ "$f" ]
                then
                        if [[ $f == *.xml* ]]
                        then
                                XML=$f
                        fi
                        if [[ $f == http* ]]
                        then
                                HOST=$f
                        fi
                        if [[ $f == *axis* ]]
                        then
                                APP=$f
                        fi
                        if [[ $f != http* ]]
                        then
                                if [[ $f != *axis* ]]
                                then
                                        if [[ $f != *.xml* ]]
                                        then
                                                CLIENT=$f/
                                        fi
                                fi
                        fi
                fi
        done
echo Testing $CLIENT with $XML at $HOST""/$APP/services/$CLIENT""ClientPort $XML
echo -------------------------------------
echo -------------------------------------
./soapclient.sh $HOST""/$APP/services/$CLIENT""ClientPort $XML
echo -------------------------------------
