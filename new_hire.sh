#! /bin/sh
#THIS SCRIPT IS TO AUTO INSTALL ALL OF MY USUAL DEVELOPMENT TOOLS.
#Written by Delicia Brummitt (dkbrummitt@gmail.com)

clear
echo Installing Programming Languayes...
echo INSTALLING JAVA...
sudo apt-get install sun-java-jdk sun-java-doc sun-java6-jre sun-java6-plugin sun-java6-fonts
sudo update-java-alternatives -s java-6-sun
sudo echo ‘JAVA_HOME=/usr/lib/jvm/java-6-sun’ >> /etc/profile

echo INSTALLING GRAILS
sudo add-apt-repository ppa:groovy-dev/grails
sudo apt-get update
sudo apt-get install grails grails-doc
sudo echo ‘GRAILS_HOME=/usr/lib/groovy to /usr/share/groovy-1.3.7’ >> /etc/profile

echo INSTALLING PYTHON
apt-get install python3.2

clear
echo Installing Database Servers
echo INSTALLING MYSQL SERVER
sudo apt-get install mysql-server
mysqladmin -u root password root

echo INSTALLING MYSQL QUERY BROWSER
sudo /etc/init.d/mysql restart
sudo apt-get install mysql-query-browser

clear
echo Installing Build Tools
echo INSTALLING MAVEN
sudo apt-get install maven2

clear
echo INSTALLING ANT
sudo apt-get install ant ant-optional

clear
echo Installing Servers
echo INSTALLING TOMCAT
sudo apt-get install tomcat6
sudo apt-get install tomcat6-common
sudo apt-get install tomcat6-webapps
sudo apt-get install tomcat6-admin

clear
echo Intalling Version Conrol
echo INSTALLING SUBVERSION CLIENT - dial out to corp SVN
sudo apt-get install subversion

echo INSTALLING SUBVERSION SERVER - local SVN
sudo apt-get install subversion
sudo mkdir /usr/local/svn
sudo mkdir /usr/local/svn/repos
sudo groupadd svn
sudo chgrp svn /usr/local/svn/repos
sudo chmod g+w /usr/local/svn/repos
sudo chmod g+s /usr/local/svn/repos

#ADD MYSELF AS SVN USER  
sudo usermod -a -G svn delicia
echo SVN INSTALL COMPLETE. YOU MUST LOG OUT AND LOG BACK IN FOR CHANGES TO TAKE EFFECT

echo INSTALL GIT
apt-get install git-core

clear
echo Installing IDEs
echo INSTALLING NETBEANS
chmod +x netbeans-6.9.1-ml-linux.sh
./myinstall2.sh netbeans-6.9.1-ml-linux.sh


clear
echo Installing Extras
echo INSTALLING HUDSON - Continuous Integration
sudo sh -c "echo 'deb http://hudson-ci.org/debian binary/' > /etc/apt/sources.list.d/hudson.list"
sudo apt-get update
sudo apt-get install hudson

################################################################
		END-OF-SCRIPT
#################################################################
